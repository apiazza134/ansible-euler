# Ansible Euler
Ansible per la configurazione del mio portatile (e di eventuali altri miei computer).

I `roles` presenti con i file più rilevanti sono
- `base-config`
  + `bashrc`
  + configurazione di [xmonad](https://xmonad.org/) (con xmobar)
- `extra-config`
  + configurazione di [GNU Emacs](https://www.gnu.org/software/emacs/) (e di `emacsclient`)
  + rescaling dello schermo con [HiDPI-Fixer](https://github.com/alex-spataru/HiDPI-Fixer)
- `latex`
  + installazione automatizzata di TeXLive se non presente
  + configurazione `latexmk`
  + configurazione `texstudio`
- `root-config`
  + configurazione `gnome-flashback-session` con xmonad
- `utils` per installazione di pacchetti

## Gnome Flashback Xmonad
Gnome Flashback è una versione light di Gnome che ci permette di avere tutte le cose comode di quel DE ma di poter sostituire il window manager che ci piace, per esempio xmonad. La configurazione però non è facile perché ci sono un po' di file da sistemare e cambia spesso nel tempo. Al momento il *workflow* è il seguente:

- `gdm` in un qualche modo mistico presenta nel menu di login le cose che stanno entro alla `/usr/share/xsessions` e in particolare quando seleziono *Gnome Flashback (Xmonad)* dovrebbe fare riferimento a `/usr/share/xsessions/gnome-flashback-xmonad.desktop`
- `/usr/share/xsessions/gnome-flashback-xmonad.desktop` lancia `/usr/libexec/gnome-flashback-xmonad` (e controlla che esista `gnome-flashback`)
- `/usr/libexec/gnome-flashback-xmonad` lancia il comando importante
  ```bash
  gnome-session --systemd --session=gnome-flashback-xmonad [...]
  ```
  + il `--session` setta il nome nella sessione e dice di usare `/usr/share/gnome-session/sessions/gnome-flashback-xmonad.session` nel quale gli diciamo i vari componenti che ci interessa caricare, tra cui xmonad, gnome-flashback e un po' di plugin di gnome (tipo quelli che fanno funzionare i pulsanti per il volume e la luminosità senza doverseli settare a mano)
  + il `--system` triggera un po' di unit standard di gnome (tipo `/usr/lib/systemd/user/gnome-session.target`, etc) e quella custom per la sessione `/usr/lib/systemd/user/gnome-session@gnome-flashback-xmonad.target.d/session.conf` nella quale gli di dice di triggerare `/usr/lib/systemd/user/gnome-flashback.target`, oltre che a caricare un po' dei componenti settati nel `.session` di prima.

## GNU Global
Nella `$HOME/.gtags/` lanciare
```bash
$ gtags -c
```
Il `-c` è importate altrimenti il database è enorme.

## Password del grub
Usaiamo il seguente comando per generare l'hash
```
# grub-mkpasswd-pbkdf2

Enter password:
Reenter password:
PBKDF2 hash of your password is <password>
```
A questo punto creiamo il file `/etc/grub.d/35_auth` in cui inseriamo l'hash appena generato
```bash
#!/bin/sh
set -e

# Grub user
echo 'set superusers="<username>"'
# Grub password
echo 'password_pbkdf2 <username> <password>'
```
e gli metteriamo i permessi di eseguibile
```
# chmod a+x /etc/grub.d/35_auth
```

In questo modo tutte le entry sono protette da password. Se voglio che solo la modifica delle opzioni di boot sia protetta da password dobbiamo modificare `/etc/grub.d/10_linux` aggiungendo l'opzione `--unrestricted` alla variabile `CLASS`
```bash
CLASS="--class gnu-linux --class gnu --class os --unrestricted"
```
Con questa opzione però sia winzoz che debian-emergency hanno bisogno di password lo stesso: è una cosa che mi va bene anche se non ho capito perché succeda.

Fatte tutte le modifiche ricordarsi di fare
```
# update-grub
# update-initramfs -u
```
Il secondo non so se è necessario ma non fa male.

## Geogebra
```bash
# chmod 4755 /usr/share/geogebra-classic/chrome-sandbox
```
