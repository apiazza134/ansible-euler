SHELL := /bin/bash

.PHONY: run check
.DEFAULT_TARGET: run
PYTHON_APT = $(shell apt-get download --print-uris python3-apt | awk '{print $$2}')
PYTHON_VERSION = $(shell python3 --version | awk -F "[ .]" '{print "python"$$2"."$$3}')
BECOME = $(shell grep -qE "# +become: yes" main.yml; echo $$?)

run: venv
ifeq ($(BECOME),0)
	source venv/bin/activate && ansible-playbook -i inventory.ini main.yml
else
	source venv/bin/activate && ansible-playbook -i inventory.ini --ask-become-pass main.yml
endif

check: venv
ifeq ($(BECOME),0)
	source venv/bin/activate && ansible-playbook -i inventory.ini --diff --check main.yml
else
	source venv/bin/activate && ansible-playbook -i inventory.ini --diff --check --ask-become-pass main.yml
endif

venv:
	virtualenv -p python3 venv
	source venv/bin/activate && pip install -r requirements.txt
	apt-get download python3-apt
	dpkg --extract $(PYTHON_APT) apt-tmp
	rm -rf venv/lib/$(PYTHON_VERSION)/site-packages/apt*
	mv apt-tmp/usr/lib/python3/dist-packages/* venv/lib/$(PYTHON_VERSION)/site-packages/
	rm -r apt-tmp $(PYTHON_APT)
