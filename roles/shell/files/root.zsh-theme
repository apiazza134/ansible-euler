function virtualenv_prompt_info {
  [[ -n ${VIRTUAL_ENV} ]] || return
  echo "${ZSH_THEME_VIRTUALENV_PREFIX:=[}${VIRTUAL_ENV:t}${ZSH_THEME_VIRTUALENV_SUFFIX:=]}"
}


local git_info='$(git_prompt_info)'
local virtualenv_info='$(virtualenv_prompt_info)'

local PROMPT_SUCCESS_COLOR=$FG[010]
local PROMPT_FAILURE_COLOR=$FG[009]
local PROMPT_DEFAULT_END=❯

local context="${FG[160]}${SHORT_HOST:-$HOST}"

local prompt_char="%(0?.%{$PROMPT_SUCCESS_COLOR%}.%{$PROMPT_FAILURE_COLOR%})$PROMPT_DEFAULT_END"

PROMPT="${virtualenv_info}${git_info}${context}:%B${FG[012]}%~%b ${prompt_char}%{$reset_color%} "

ZSH_THEME_GIT_PROMPT_PREFIX="${FG[160]}("
ZSH_THEME_GIT_PROMPT_SUFFIX=")%{$reset_color%} "
ZSH_THEME_GIT_PROMPT_DIRTY=""
ZSH_THEME_GIT_PROMPT_CLEAN=""

export VIRTUAL_ENV_DISABLE_PROMPT=1
ZSH_THEME_VIRTUALENV_PREFIX="${FG[007]}("
ZSH_THEME_VIRTUALENV_SUFFIX=")%{$reset_color%} "
