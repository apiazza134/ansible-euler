;;; init-spell.el -- spelling
;;; Commentary:

;;; Code:
(use-package flyspell
  :diminish
  :if (executable-find "aspell")
  :hook (((outline-mode latex-mode org-mode markdown-mode) . flyspell-mode))
  :custom
  (ispell-program-name "aspell")
  ;; (ispell-dictionary "italiano")
  (ispell-extra-args '("--sug-mode=ultra"))
  (add-to-list 'company-backends 'company-ispell))

;; (add-to-list 'load-path "/home/alepiazza/.emacs.d/Emacs-langtool")
;; ;; (setq langtool-java-classpath )
;; (setq langtool-language-tool-jar "/home/alepiazza/LanguageTool-5.9-stable/languagetool-commandline.jar"
;;       langtool-java-classpath "/home/alepiazza/LanguageTool-5.9-stable"
;; 	  langtool-java-bin "/usr/bin/java"
;; 	  langtool-default-language "en-GB"
;; 	  langtool-mother-tongue "en"
;; 	  langtool-java-user-arguments '("-Dfile.encoding=UTF-8"))

;; (require 'langtool)


(provide 'init-spell)
;;; init-spell ends here
