;;; init-markdown.el -- markdown mode
;;; Commentary:

;;; Code:
(use-package markdown-mode
  :hook
  (markdown-mode . (lambda () (yas-activate-extra-mode 'latex-mode)))
  :custom
  (markdown-command "pandoc -s")
  (markdown-enable-math t))

(provide 'init-markdown)
;;; init-markdown ends here
