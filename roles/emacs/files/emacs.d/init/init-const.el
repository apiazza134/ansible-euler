;;; init-const.el -- various globabl constants
;;; Commentary:

;;; Code:
(defvar ap/repos-usual-path "~/Desktop/repos") ;; not working
;; ~/org/agenda/* for recentf-exclude
;; (defvar ap/org-directory "~/org/")
;; (defvar ap/org-agenda-directory (concat ap/org-directory "agenda/"))
;; (setq org-agenda-files (directory-files-recursively "~/org/" "\.org$"))
;; (defvar ap/org-agenda-todo (concat ap/org-agenda-directory "todo.org"))
;; (defvar ap/org-agenda-journal (concat ap/org-agenda-directory "journal.org"))

;; (setq org-directory ap/org-directory
;;       org-agenda-files ap/org-agenda-files
;;       org-archive-location (concat org-directory "archive/%s_archive::")
;;       ;; capture template config
;;       org-capture-templates
;;        '(("t" "Todo item" entry (file+headline ap/org-agenda-todo "Tasks")
;;           "* TODO %?\nAdded %U\n %i")
;;          ("s" "Todo item with schedule" entry (file+headline ap/org-agenda-todo "Tasks")
;;           "* TODO %?\nSCHEDULED: %t\nAdded %U\n %i")
;;          ("d" "Todo item with deadline" entry (file+headline ap/org-agenda-todo "Tasks")
;;           "* TODO %?\nDEADLINE: %t\nAdded %U\n %i")
;;          ("j" "Journal entry" entry (file+datetree ap/org-agenda-journal)
;;           "* %?\nAdded %U\n %i")))

(use-package exec-path-from-shell
  :custom
  (exec-path-from-shell-variables '("PATH" "MANPATH" "SSH_AUTH_SOCK" "GTAGSLIBPATH" "GTAGSTROUGH"))
  (exec-path-from-shell-check-startup-files nil)
  :config
  (exec-path-from-shell-initialize))

;; (use-package org-gcal
;;   :custom
;;   (org-gcal-client-id "892124844542-j85bs32067b7l0vkkrmo9u1onr1qbrtd.apps.googleusercontent.com")
;;   (org-gcal-up-days 60)
;;   (org-gcal-down-days 90)
;;   (org-gcal-file-alist '(("alessandro.piazza@sns.it" . "~/org/agenda/google-calendar/sns.org")
;;                          ("sns.it_tddne1ld5o8d9r1225me4et28g@group.calendar.google.com" . "~/org/agenda/google-calendar/esami.org")))
;;   :config
;;   (load "~/.emacs.d/gcal-client-secret.el")
;;   ;; (add-hook 'org-agenda-mode-hook 'org-gcal-fetch)
;;   ;; (add-hook 'org-capture-after-finalize-hook 'org-gcal-sync)
;;   )

(provide 'init-const)
;;; init-const ends here
