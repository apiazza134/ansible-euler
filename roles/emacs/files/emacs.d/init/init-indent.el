;;; init-indent.el -- indent
;;; Commentary:

;;; Code:
;; (use-package highlight-indent-guides
;;   :diminish
;;   :hook (prog-mode . highlight-indent-guides-mode)
;;   :custom-face
;;   (highlight-indent-guides-character-face ((t :foreground "dimgray")))
;;   :custom
;;   (highlight-indent-guides-method 'character)
;;   (highlight-indent-guides-responsive 'top)
;;   (highlight-indent-guides-delay 0))

(setq-default indent-tabs-mode nil)
(setq-default indent-line-function 'insert-tab)
(setq-default tab-width 4)

(provide 'init-indent)
;;; init-indent ends here
