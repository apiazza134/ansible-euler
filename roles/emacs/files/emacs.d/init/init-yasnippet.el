;;; init-yasnippet.el -- snippets configuration
;;; Commentary:

;;; Code:
(use-package yasnippet
  :diminish yas-minor-mode
  :init (use-package yasnippet-snippets :after yasnippet)
  :hook ((prog-mode LaTeX-mode org-mode) . yas-minor-mode)
  :bind
  (:map yas-keymap (("TAB" . smarter-yas-expand-next-field)))
  :custom
  (yas-triggers-in-field t)
  :config
  (yas-global-mode 1)

  (defun smarter-yas-expand-next-field ()
    "Try to `yas-expand' then `yas-next-field' at current cursor position."
    (interactive)
    (let ((old-point (point))
          (old-tick (buffer-chars-modified-tick)))
      (yas-expand)
      (when (and (eq old-point (point))
                 (eq old-tick (buffer-chars-modified-tick)))
        (ignore-errors (yas-next-field))))))

(provide 'init-yasnippet)
;;; init-yasnippet ends here
