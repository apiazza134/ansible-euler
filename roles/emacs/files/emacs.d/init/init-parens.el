;;; init-parens.el -- parenthese
;;; Commentary:

;;; Code:
(electric-pair-mode 1)
(show-paren-mode 1)
;; (setq show-paren-delay 0)



;; (use-package smartparens
;;   :hook (prog-mode . smartparens-mode)
;;   :diminish smartparens-mode
;;   :custom
;;   (sp-escape-quotes-after-insert nil)
;;   :config
;;   ;; Stop pairing single quotes in elisp
;;   (sp-local-pair 'emacs-lisp-mode "'" nil :actions nil)
;;   (sp-local-pair 'org-mode "[" nil :actions nil))

(provide 'init-parens)
;;; init-parens ends here
