;;; init-org.el -- org mode and agenda
;;; Commentary:

;;; Code:
(use-package org
  :ensure nil
  :bind (("M-a" . 'org-agenda-list)
         ("C-c a" . 'org-agenda)
         ("C-c c" . 'org-capture)
         ;; (:map org-mode-map ("C-c C-a C-d" . 'org-mark-done-and-archive))
         )
  (:map org-mode-map  ("<f5>" . 'org-latex-export-to-pdf))
  :hook
  (org-mode . visual-line-mode)
  (org-mode . (lambda () (yas-activate-extra-mode 'latex-mode)))
  (org-mode . (lambda ()
                (setq-local electric-pair-pairs
                            (append electric-pair-pairs org-electric-paris))))
  ;; TODO: Non funziona e dà errori a caso
  ;; (org-mode . (lambda ()
  ;;               (setq-local ispell-skip-region-alist
  ;;                           (append ispell-skip-region-alist ispell-tex-skip-alists))))
  (org-mode . (lambda () (add-to-list 'org-latex-compilers "latexmk")))
  (org-archive-hook . org-save-all-org-buffers)
  :custom
  ;; generic config
  (org-startup-folded 'content)
  (org-cycle-separator-lines 1)
  (org-startup-indented t)
  (org-ellipsis "⤵")
  (org-support-shift-select t)
  ;; todo config
  (org-todo-keywords '((sequence "TODO" "IN-PROGRESS" "|" "DONE" "CANCELED")))
  (org-enforce-todo-dependencies t)
  (org-enforce-todo-checkbox-dependencies t)
  (org-enforce-todo-dependencies t)
  ;; log config
  (org-log-done 'time)
  (org-log-redeadline 'time)
  (org-log-reschedule 'time)

  ;; src config
  (org-src-window-setup 'current-window)
  (org-src-fontify-natively t)
  (org-src-tab-acts-natively t)
  ;; latex config
  (org-latex-compiler "latexmk")
  (org-latex-pdf-process '("%latex -shell-escape -output-directory=%o %f"))
  (org-latex-tables-centered t)
  (org-highlight-latex-and-related '(latex script entities))
  (org-latex-listings 'minted)
  ;; org-latex-minted-options '(("frame" "lines") ("linenos=true"))
  (org-latex-default-packages-alist
   '(("AUTO" "inputenc" t ("pdflatex"))
     ("T1" "fontenc" t ("pdflatex"))
     ("" "graphicx" t nil)
     ("" "grffile" t nil)
     ("" "longtable" nil nil)
     ("" "wrapfig" nil nil)
     ("" "rotating" nil nil)
     ("normalem" "ulem" t nil)
     ("" "textcomp" t nil)
     ("bookmarks,colorlinks,linkcolor=blue" "hyperref" nil nil)
     ("" "booktabs")
     ("" "enumitem")
     ("" "capt-of" nil nil)
     ("" "amsmath" t nil)
     ("" "amssymb" t nil)
     ("" "amsfonts" t nil)
     ("" "mathrsfs")
     ("" "mathtools")
     ("" "commath")
     ("" "halloweenmath")))
  :config
  ;; electric-paris
  (defvar org-electric-paris '((?/ . ?/) (?$ . ?$)) "Electric pairs for org-mode")
  ;; utility functions
  (defun org-mark-done-and-archive ()
    (interactive)
    (org-todo 'done)
    (org-archive-subtree))
  (defun org-archive-all-done-tasks ()
    (interactive)
    (org-map-entries 'org-archive-subtree "/DONE" 'file))
  (defun org-archive-all-canceled-tasks ()
    (interactive)
    (org-map-entries 'org-archive-subtree "/CANCELED" 'file))
  )

(use-package org-bullets
  :init (add-hook 'org-mode-hook (lambda () (org-bullets-mode 1))))

(use-package ox-twbs
  :custom
  ;; set html and bootstrap export templates and options
  (org-html-mathjax-template org-twbs-mathjax-template)
  (org-html-mathjax-options org-twbs-mathjax-options))

(setq org-twbs-mathjax-template
      "<script>
    window.MathJax = {
      startup: {
        ready: () => {
          document.body.innerHTML = document.body.innerHTML.replace(/dcases/g, 'cases');
          MathJax.startup.defaultReady();
        }
      },
      svg: {
        displayAlign: \"%ALIGN\",
        displayIndent: \"%INDENT\",
        scale: %SCALE
      },
      chtml: {
        scale: %SCALE
      },
      options: {
        ignoreHtmlClass: 'tex2jax_ignore',
        processHtmlClass: 'tex2jax_process'
      },
      tex: {
        autoload: {
          color: [],
          colorV2: ['color']
        },
        packages: {'[+]': ['noerrors', 'ams', 'braket']},
        tags: 'ams',
        macros: {
          dif: ['\\\\operatorname{d} \\\\!'],
          dod: ['\\\\dfrac{\\\\dif{^{#1}}#2}{\\\\dif{#3^{#1}}}', 3, ''],
          dpd: ['\\\\dfrac{\\\\partial{^{#1}} \\\\! #2}{\\\\partial{\\\\! #3^{#1}}}', 3, ''],
          abs: ['\\\\left|#1\\\\right|', 1],
          del: ['\\\\left(#1\\\\right)', 1],
          sbr: ['\\\\left[#1\\\\right]', 1],
          cbr: ['\\\\left\\\\{#1\\\\right\\\\}', 1],
          coloneqq: ['\\\\mathrel{\\\\vcenter{:}}='],
          eqqcolon: ['=\\\\mathrel{\\\\vcenter{:}}'],
          ped: ['_{\\\\text{#1}}', 1],
          ap: ['^{\\\\text{#1}}', 1],
          N: ['\\\\mathbb{N}'],
          R: ['\\\\mathbb{R}'],
          C: ['\\\\mathbb{C}'],
        }
      },
      loader: {
        load: ['[tex]/noerrors', '[tex]/ams', '[tex]/braket']
        }
      };
  </script>
  <script id=\"MathJax-script\" async src=\"%PATH\"></script>
")
(setq org-twbs-mathjax-options
      '(
        (path "https://cdn.jsdelivr.net/npm/mathjax@3.0.1/es5/tex-svg.js")
        (scale "1")
        (dscale "1")
        (align "center")
        (indent "2em")
        (messages "none")
        )
      )

(use-package htmlize)
(use-package toc-org
  :hook (org-mode . toc-org-mode))

;; Installato ma da errori a caso https://emacs.stackexchange.com/questions/37036/where-are-these-variables-defined-bytecomp-warnings
;; (use-package oauth2 :ensure t :defer t)

(use-package calfw
  :custom
  (calendar-week-start-day 1)
  (cfw:render-line-breaker 'cfw:render-line-breaker-wordwrap)
  (cfw:fchar-junction ?╬)
  (cfw:fchar-vertical-line ?║)
  (cfw:fchar-horizontal-line ?═)
  (cfw:fchar-left-junction ?╠)
  (cfw:fchar-right-junction ?╣)
  (cfw:fchar-top-junction ?╦)
  (cfw:fchar-top-left-corner ?╔)
  (cfw:fchar-top-right-corner ?╗)
  :custom-face
  (cfw:face-toolbar ((nil)))
  (cfw:face-toolbar-button-off ((t :foreground "DarkGray" :weight bold)))
  (cfw:face-toolbar-button-on ((t :foreground "LightGray" :weight bold)))
  (cfw:face-header ((t :foreground "LightGray")))
  (cfw:face-saturday ((t :foreground "#cd950c")))
  (cfw:face-sunday ((t :foreground "#cd950c")))
  (cfw:face-holiday ((t :background "DarkGreen" :foreground "white")))
  (cfw:face-day-title ((t :background "grey27")))
  (cfw:face-select ((t :background "orange3")))
  (cfw:face-today-title ((t :background "SlateBlue")))
  ;; (cfw:face-today ((t :background "cyan3" :weight bold)))
  )

(use-package calfw-org
  :bind ("M-c" . cfw:open-org-calendar)
  :custom
  (cfw:org-overwrite-default-keybinding t)
  (cfw:org-face-agenda-item-foreground-color "PowderBlue"))

(provide 'init-org)
;;; init-org ends here
