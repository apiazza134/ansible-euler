;;; init-company.el -- autocompletion with company
;;; Commentary:

;;; Code:
(use-package company
  :bind
  (:map company-active-map ("TAB" . smarter-tab-to-complete))
  :init
  (use-package company-statistics
    :config (company-statistics-mode))
  (if (display-graphic-p)
        (use-package company-quickhelp
          :config (company-quickhelp-mode)))

  :custom
  (company-minimum-prefix-length 1)
  (company-require-match 'never)
  (company-global-modes '(not shell-mode eaf-mode))
  (company-idle-delay 0)
  (company-show-numbers t)
  (company-auto-commit nil)
  (company-auto-commit-chars nil)
  (company-dabbrev-other-buffers nil)
  (company-dabbrev-ignore-case nil)
  (company-dabbrev-downcase nil)
  (company-tooltip-align-annotations t)
  (company-tooltip-flip-when-above t)
  (company-tooltip-limit 14)
  (company-tooltip-align-annotations t)

  :config
  (global-company-mode 1)

  (defun smarter-tab-to-complete ()
    "Try to `org-cycle', `yas-expand', and `yas-next-field' at current cursor position. If all failed, try to complete the common part with `company-complete-common'"
    (interactive)
    (when yas-minor-mode
      (let ((old-point (point))
            (old-tick (buffer-chars-modified-tick))
            (func-list
             (if (equal major-mode 'org-mode) '(org-cycle yas-expand yas-next-field)
               '(yas-expand yas-next-field))))
        (catch 'func-suceed
          (dolist (func func-list)
            (ignore-errors (call-interactively func))
            (unless (and (eq old-point (point))
                       (eq old-tick (buffer-chars-modified-tick)))
              (throw 'func-suceed t)))
          (company-complete-common)))))
  )

(use-package company-tabnine
  :hook (kill-emacs . company-tabnine-kill-process)
  :custom (company-tabnine-binaries-folder (expand-file-name "tabnine" user-emacs-directory))
  :config
  (add-to-list 'company-backends #'company-tabnine))

;; (use-package semantic
;;   :config
;;   (global-semanticdb-minor-mode 1)
;;   (global-semantic-idle-scheduler-mode 1)
;;   ;; (global-semantic-idle-summary-mode 1)
;;   (semantic-mode 1))

(provide 'init-company)
;;; init-company ends here
