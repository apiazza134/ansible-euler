;;; init-cc.el -- c/c++ mode configuration
;;; Commentary:

;;; Code:
(add-hook 'c-mode-hook (lambda () (c-set-style "stroustrup")))
(add-hook 'c++-mode-hook (lambda () (c-set-style "stroustrup")))

(use-package cmake-ide
  ;; :bind* (:map c-mode-map ("C-c C-c" . cmake-ide-compile))
  :config (cmake-ide-setup)
  )

(use-package ggtags
  ;:bind (:map ggtags-mode-map ;; ("C-c g s" . ggtags-find-other-symbol)
  ;;             ("C-c g h" . ggtags-view-tag-history)
  ;;             ("C-c g r" . ggtags-find-reference)
  ;;             ("C-c g d" . ggtags-find-definition)
  ;;             ("C-c g f" . ggtags-find-file)
  ;;             ("C-c g c" . ggtags-create-tags)
  ;;             ("C-c g u" . ggtags-update-tags)
  ;;             ("M-," . pop-tag-mark))
  ;; :config
  ;; (setq imenu-create-index-function #'ggtags-build-imenu-index
  ;;       eldoc-documentation-function #'ggtags-eldoc-function)
  )

;; (add-hook 'c-mode-common-hook
          ;; (lambda ()
          ;;   (when (derived-mode-p 'c-mode 'c++-mode 'java-mode 'asm-mode)
          ;;     (ggtags-mode t))))

(use-package irony
  :init
  (use-package irony-eldoc
    :hook (irony-mode . irony-eldoc))
  (use-package company-irony
    :hook (irony-mode . (lambda () (setq-local company-backends (append company-backends '(company-irony))))))
  (use-package company-irony-c-headers
    :hook (irony-mode . (lambda () (setq-local company-backends (append company-backends '(company-irony-c-headers))))))
  (use-package flycheck-irony
    :hook (irony-mode . flycheck-irony-setup)
    :custom (flycheck-gcc-language-standard "c++20"))

  :config
  (add-hook 'c-mode-hook 'irony-mode)
  (add-hook 'c++-mode-hook 'irony-mode)
  (add-hook 'irony-mode-hook 'irony-cdb-autosetup-compile-options))

(provide 'init-cc)
;;; init-cc ends here
