;;; init-latex.el -- latex mode configuration
;;; Commentary:

;;; Code:
(use-package tex
  :ensure auctex ;; il nome del pacchetto è diverso
  :bind (:map TeX-mode-map ("<f5>" . my-LatexMk-compile)
              ("\"" . tex-insert-quote)
              ("C-c C-v" . TeX-view)
              ("C-c h" . outline-hide-body))
  :hook
  (TeX-mode . outline-minor-mode)
  (TeX-mode . TeX-source-correlate-mode)
  :custom
  (TeX-auto-save t)
  (TeX-parse-self t)
  (TeX-brace-indent-level 4)
  (TeX-electric-sub-and-superscript t)
  (TeX-electric-math (cons "\\( " " \\)"))
  (LaTeX-electric-left-right-brace t)
  (LaTeX-indent-level 4)
  (LaTeX-item-indent 0)
  (TeX-view-program-selection '((output-pdf "Evince")))
  (TeX-source-correlate-start-server t)
  :config
  (add-hook 'TeX-after-compilation-finished-functions 'TeX-revert-document-buffer)
  (defun my-LatexMk-compile ()
    "Compile latex with latexmk"
    (interactive)
    (save-buffer)
    (TeX-command "LatexMk" 'TeX-master-file))

  (defun decentify ()
    "Decentify buffer"
    (interactive)
    (save-excursion
      (untabify 0 (buffer-size))
      (delete-trailing-whitespace)
      (beginning-of-buffer)
      (push-mark)
      (setq mark-active t)
      (end-of-buffer)
      (indent-for-tab-command)
      (deactivate-mark)))
  )

(use-package bibtex
  :custom
  (bibtex-align-at-equal-sign t)
  (bibtex-text-indentation 14))

(use-package auctex-latexmk
  :init (auctex-latexmk-setup)
  :custom
  (auctex-latexmk-inherit-TeX-PDF-mode t))

(use-package company-auctex
  :hook (TeX-mode . (lambda () (make-local-variable company-backends) (company-auctex-init))))

(use-package company-math
  :hook (TeX-mode . (lambda () (setq-local company-backends
                                      (append company-backends '(company-math-symbols-latex company-latex-commands))))
                  ))

(provide 'init-latex)
;;; init-latex ends here
