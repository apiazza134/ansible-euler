;;; init-cc.el -- fortran mode configuration
;;; Commentary:

;;; Code:
(use-package fortran
  :ensure nil
  :config
  (setq flycheck-gfortran-language-standard "f2008"
        flycheck-gfortran-args '("-cpp")))

(add-to-list 'safe-local-variable-values '(eval flycheck-gfortran-args))

(provide 'init-fortran)
;;; init-fortran ends here
