;;; init-web.el -- html, js mode
;;; Commentary:

;;; Code:
(use-package web-mode
  :mode (("\\.html?\\'" . web-mode)
         ("\\.tsx\\'" . web-mode)
         ("\\.js\\'" . web-mode)
         ("\\.jsx\\'" . web-mode))
  :custom
  (web-mode-markup-indent-offset 2)
  (web-mode-css-indent-offset 2)
  (web-mode-code-indent-offset 2)
  (web-mode-block-padding 2)
  (web-mode-comment-style 2)

  (web-mode-enable-css-colorization t)
  (web-mode-enable-auto-pairing t)
  (web-mode-enable-auto-closing t)
  (web-mode-enable-comment-keywords t)
  (web-mode-enable-current-element-highlight t)

  :config
  ;; enable typescript-tslint checker
  (flycheck-add-mode 'typescript-tslint 'web-mode))

(use-package company-web
  :hook (web-mode . (lambda () (setq-local company-backends (append company-backends '(company-web-html))))))

(use-package typescript-mode
  ;; :mode ("\\.tsx\\'" . typescript-mode)
  :hook subword-mode
  :custom (typescript-indent-level 2))

(use-package tide
  :hook ((typescript-mode . tide-setup)
         (typescript-mode . tide-hl-identifier-mode)
         (before-save . tide-format-before-save)))

(use-package css-mode
  :init
  (use-package rainbow-mode
    :diminish
    :hook (css-mode . rainbow-mode)))


(provide 'init-web)
;;; init-web ends here
