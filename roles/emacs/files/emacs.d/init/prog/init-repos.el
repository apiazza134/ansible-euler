;;; init-repos.el -- repository
;;; Commentary:

;;; Code:
(use-package magit
  :bind (("C-x g" . magit-status)))

(use-package projectile
  :custom
  (projectile-project-search-path '("~/Desktop/repos"))
  (projectile-enable-caching t)
  (projectile-sort-order 'recentf)
  (projectile-completion-system 'ivy)
  :config
  (projectile-mode 1)
  (define-key projectile-mode-map (kbd "C-c C-p") 'projectile-command-map))

(use-package neotree
  :bind ([f8] . neotree-project-dir)
  :custom
  (neo-theme (if (display-graphic-p) 'icons 'arrow))
  (neo-smart-open t)
  (projectile-switch-project-action 'neotree-projectile-action)
  :config
  (defun neotree-project-dir ()
    "Open NeoTree using the git root."
    (interactive)
    (let ((project-dir (projectile-project-root))
          (file-name (buffer-file-name)))
      (neotree-toggle)
      (if project-dir
          (if (neo-global--window-exists-p)
              (progn
                (neotree-dir project-dir)
                (neotree-find file-name)))
        (message "Could not find git project root.")))))

(provide 'init-repos)
;;; init-repos ends here
