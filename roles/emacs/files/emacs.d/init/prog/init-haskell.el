;;; init-haskell -- haskell mode config
;;; Commentary:

;;; Code:
(use-package haskell-mode)

(use-package flycheck-haskell
  :hook (haskell-mode . flycheck-haskell-setup))

(provide 'init-haskell)
;;; init-haskell ends here
