;;; init-quickrun -- quickrun config
;;; Commentary:

;;; Code:
(use-package quickrun
  :bind
  (("<f5>" . quickrun)
   ("M-<f5>" . quickrun-shell)))

(provide 'init-quickrun)
;;; init-quickrun ends here
