;;; init-python.el -- python mode configuration
;;; Commentary:

;;; Code:
(use-package python
  :custom
  (python-shell-interpreter "python3")
  (python-indent-offset 4))

(use-package elpy
  :hook (python-mode . elpy-enable)
  :commands (elpy-rpc-get-virtualenv-path)
  :custom
  (elpy-rpc-python-command "python")
  (flycheck-python-pycompile-executable (concat (elpy-rpc-get-virtualenv-path) "/bin/python3"))
  (flycheck-python-flake8-executable (concat (elpy-rpc-get-virtualenv-path) "/bin/flake8"))
  :config
  (remove-hook 'elpy-modules 'elpy-module-flymake)
  (electric-indent-local-mode 0)
  ;; (advice-add '(lambda () (pyvenv-activate (elpy-rpc-default-virtualenv-path))) #'elpy-config))
  )

(use-package pyvenv
  :config
  (setq pyvenv-default-virtual-env-name "venv"))

(use-package company-jedi
  :hook (python-mode . (lambda () (setq-local company-backends (append company-backends '(company-jedi))))))

(provide 'init-python)
;;; init-python ends here
