;;; init-syntax.el -- syntax checkers
;;; Commentary:

;;; Code:
(use-package flymake :disabled t)
(use-package flycheck
  :diminish
  :hook (after-init . global-flycheck-mode)
  :init
  (if (display-graphic-p)
      (use-package flycheck-posframe
        :after flycheck
        :hook (flycheck-mode . flycheck-posframe-mode)
        :custom-face
        (flycheck-posframe-warning-face ((t :inherit 'warning)))
        (flycheck-posframe-error-face ((t :inherit 'error)))
        :custom
        (flycheck-posframe-border-width 3)
        (flycheck-posframe-warning-prefix "! ")
        (flycheck-posframe-error-prefix "\u2718 ")
        (flycheck-posframe-inhibit-functions '((lambda (&rest _) (bound-and-true-p company-backend))))))

  :custom
  (flycheck-global-modes
   '(not outline-mode diff-mode shell-mode eshell-mode term-mode))
  (flycheck-emacs-lisp-load-path 'inherit))

(provide 'init-syntax)
;;; init-syntax.el ends here
