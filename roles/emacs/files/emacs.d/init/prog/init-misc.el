;;; init-misc.el -- miscellaneus programming modes
;;; Commentary:

;;; Code:
(use-package json-mode
  :mode ("\\.json\\'" . json-mode)
  :custom (js-indent-level 4))

(use-package lua-mode
  :mode (("\\.lua\\'" . lua-mode)))

(use-package gnuplot
  :mode (("\\.gp\\'" . gnuplot-mode))
  :bind (:map gnuplot-mode-map ("C-c C-c" . gnuplot-send-buffer-to-gnuplot))
  :hook
  (gnuplot-mode . gnuplot-context-sensitive-mode)
  (gnuplot-mode . gnuplot-inline-display-mode)
  :custom (gnuplot-eldoc-mode t))

(use-package yaml-mode
  :hook (yaml-mode . (lambda () (flyspell-mode -1))))

(use-package nginx-mode)
(use-package company-nginx
  :hook (nginx-mode . (lambda () (setq-local company-backends
                                        (append company-backends '(company-nginx))))
                    ))

(use-package dokuwiki-mode
  :mode (("\\.dokuwiki\\'" . dokuwiki-mode)))

(use-package systemd)
(use-package apache-mode)
(use-package flymd)
(use-package nginx-mode)
(use-package jinja2-mode)
(use-package crontab-mode)
(use-package dockerfile-mode)
(use-package ansible-doc)
(use-package company-ansible)
(use-package graphviz-dot-mode)

(use-package dumb-jump
  :hook (xref-backend-functions . dumb-jump-xref-activate)
  :custom
  (dumb-jump-prefer-searcher 'rg))

(provide 'init-misc)
;;; init-misc ends here
