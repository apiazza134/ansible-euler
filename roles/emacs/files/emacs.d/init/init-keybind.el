;;; init-keybind -- global keybindings
;;; Commentary:

;;; Code:
(defun mouse-start-rectangle (start-event)
  (interactive "e")
  (deactivate-mark)
  (mouse-set-point start-event)
  (rectangle-mark-mode +1)
  (let ((drag-event))
    (track-mouse
      (while (progn
               (setq drag-event (read-event))
               (mouse-movement-p drag-event))
        (mouse-set-point drag-event)))))

(bind-keys*
 ("C-t" . comment-line) ;; toggle comment
 ("C-S-<down-mouse-1>" . mouse-start-rectangle) ;; mouse rectangle mark mode
 ("C-x C-b" . ibuffer) ;; use ibuffer instead of menu-buffer
 ("<f9>" . window-swap-states) ;; swap windows
 )

;; Move around windows with M-<arrows>
(bind-keys*
 ("M-<left>" . windmove-left)
 ("M-<right>" . windmove-right)
 ("M-<up>" . windmove-up)
 ("M-<down>" . windmove-down))

(provide 'init-keybind)
;;; init-keybind ends here
