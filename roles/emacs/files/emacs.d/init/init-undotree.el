;;; init-undotree.el -- undotree
;;; Commentary:

;;; Code:
(use-package undo-tree
  :diminish
  :bind (("C-x C-u" . undo-tree-undo)
         ;; ("C-x C-u" . undo-tree-visualize)
         )
  :init (global-undo-tree-mode)
  :config (setq undo-tree-auto-save-history nil))

(provide 'init-undotree)
;;; init-undotree ends here
