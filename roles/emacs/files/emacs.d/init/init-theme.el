;;; init-theme.el -- theme
;;; Commentary:

;;; Code:
(use-package doom-themes
  :init
  ;; (load-theme 'doom-old-hope t)
  (load-theme 'doom-one t)
  :custom-face
  ;; (default ((t :height 117)))
  (vertical-border ((t :foreground "dark gray" :background "dark gray")))

  ;; doom-old-hope specific
  ;; (show-paren-match ((t (:foreground "gold" :background "gray29" :extend t :weight ultra-bold))))
  ;; (font-lock-variable-name-face ((t (:foreground "#69c94f"))))

  ;; doom-one specific
  (default ((t :background "#1a1b1e" :height 113)))
  (show-paren-match ((t (:foreground "gold" :background "#42444a" :extend t :weight ultra-bold))))
  :config
  (defun switch-theme ()
    "An interactive funtion to switch themes."
    (interactive)
    (disable-theme (intern (car (mapcar #'symbol-name custom-enabled-themes))))
    (call-interactively #'load-theme))
  )

(use-package doom-modeline
  :init
  (use-package all-the-icons)
  :custom-face
  (mode-line ((t :font "DejaVu Sans Mono-11" :background "#391859")))
  :custom
  (doom-modeline-minor-modes t)
  (doom-modeline-icon t)
  (doom-modeline-major-mode-color-icon t)
  (doom-modeline-height 25)
  (doom-modeline-project 'projectile)
  :config
  (doom-modeline-mode))

(provide 'init-theme)
;;; init-theme ends here
