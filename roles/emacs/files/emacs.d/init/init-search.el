;;; init-search.el -- search tools
;;; Commentary:

;;; Code:
(use-package ivy
  :diminish
  ;; :custom-face
  ;; (ivy-current-match ((t (:foreground "gold" :background "gray29" :extend t))))
  :init
  (use-package ivy-rich
    :init (ivy-rich-mode 1)
    :custom
    (ivy-rich-path-style 'abbrev)
    :config
    (setcdr (assq t ivy-format-functions-alist) #'ivy-format-function-line))
  (use-package all-the-icons-ivy-rich
    :init (all-the-icons-ivy-rich-mode 1))
  :bind
  (("C-x b" . ivy-switch-buffer))
  :custom
  (ivy-use-virtual-buffers t)
  (ivy-height 5)
  (ivy-on-del-error-function nil)
  (ivy-magic-slash-non-match-action 'ivy-magic-slash-non-match-create)
  (ivy-count-format "【%d/%d】")
  (ivy-wrap t))

(use-package swiper
  :bind (("C-s" . swiper-isearch)))

(use-package counsel
  :diminish
  :bind
  (("M-x" . counsel-M-x)
   ("C-h o" . counsel-describe-symbol)
   ("C-c g" . counsel-git)
   ("C-c j" . counsel-git-grep))
  :config (counsel-mode 1))

;; (use-package ido
;;   :init (ido-mode 1)
;;   ;; :bind (("DEL" . '(lambda () (backward-delete-char 1))))
;;   :custom
;;   (ido-enable-flex-matching t)
;;   (ido-enable-regexp t)
;;   (ido-enable-tramp-completion t)
;;   (ido-everywhere t)
;;   (ido-use-filename-at-point 'guess))

;; (defun ido-backspace ()
;;   "Forward to `backward-delete-char'.
;; On error (read-only), quit without selecting."
;;   (interactive)
;;   (condition-case nil
;;       (backward-delete-char 1)
;;     (error
;;      (minibuffer-keyboard-quit))))
;; (define-key ido-common-completion-map (kbd "DEL") 'ido-backspace)

;; (use-package flx)
;; (use-package flx-ido
  ;; :init (flx-ido-mode 1))

(use-package smex
  :custom
  (smex-history-lenght 15))

(provide 'init-search)
;;; init-search ends here
