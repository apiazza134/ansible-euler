;;; init-buffer.el -- buffer config
;;; Commentary:

;;; Code:
(use-package ibuffer
  :ensure nil
  :bind ("C-x C-b" . ibuffer)
  :init
  (use-package ibuffer-vc
    :commands (ibuffer-vc-set-filter-groups-by-vc-root)
    :custom
    (ibuffer-vc-skip-if-remote 'nil))
  )

(provide 'init-buffer)
;;; init-buffer ends here
