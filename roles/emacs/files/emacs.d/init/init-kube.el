;;; init-kube.el -- kubernetes configuration
;;; Commentary:

;;; Code:
(use-package kubernetes
  :commands (kubernetes-overview))

(use-package kubernetes-helm)
;; (use-package kubernetes-tramp)
(use-package kubedoc)

(provide 'init-kube)
;;; init-kube ends here
