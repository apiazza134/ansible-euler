;;; init-conf.el -- global settings
;;; Commentary:

;;; Code:
;; (set-selection-coding-system 'utf-8)
;; (prefer-coding-system 'utf-8)
;; (set-language-environment "UTF-8")
;; (set-default-coding-systems 'utf-8)
;; (set-terminal-coding-system 'utf-8)
;; (set-keyboard-coding-system 'utf-8)
;; (setq locale-coding-system 'utf-8)

;; (when (display-graphic-p)
;;   (setq x-select-request-type '(UTF8_STRING COMPOUND_TEXT TEXT STRING)))

(add-hook 'before-save-hook #'delete-trailing-whitespace)

(fset 'yes-or-no-p 'y-or-n-p)
(setq use-dialog-box nil)

(setq inhibit-startup-screen t)
(setq initial-major-mode 'text-mode)

(setq scroll-step 1)
(setq scroll-margin 1)
;; (setq scroll-conservatively 101)
;; (setq scroll-up-aggressively 0.01)
;; (setq scroll-down-aggressively 0.01)
(setq auto-window-vscroll nil)
(setq fast-but-imprecise-scrolling nil)
(setq mouse-wheel-scroll-amount '(1 ((shift) . 1)))
(setq mouse-wheel-progressive-speed nil)
(setq hscroll-step 1)
(setq hscroll-margin 1)

(setq backup-directory-alist `(("." . ,(expand-file-name "saves" user-emacs-directory))))
(setq custom-file (expand-file-name "custom-set-variables.el" user-emacs-directory))

(setq auth-source-save-behavior nil)

(use-package pretty-mode
  :custom
  (global-pretty-mode t)
  :config
  (pretty-activate-groups '(:greek :types :parentheses :superscripts)))

(use-package crux
  :bind (("C-k" . crux-smart-kill-line)
         ("C-d" . crux-duplicate-current-line-or-region)
         ("C-S-d" . crux-duplicate-and-comment-current-line-or-region)
         ("C-c r" . crux-rename-file-and-buffer)
         ("C-c i" . crux-find-user-init-file)
         ("C-c s" . crux-find-shell-init-file)
         ("C-<backspace>" . kill-word-backwards)
         )
  :config
  (defun kill-word-backwards (arg)
    "Kill word backwards and adjust the indentation. Based on http://ergoemacs.org/emacs/emacs_kill-ring.html"
    (interactive "p")
    (delete-region (point) (progn (forward-word (- arg)) (point))))
  )

(use-package winner
  :custom
  (winner-boring-buffers
   '("*Completions*"
     "*Compile-Log*"
     "*inferior-lisp*"
     "*Help*"
     "*Buffer List*"
     "*Ibuffer*"
     "*Messages*"
     "*Completions*"))
  :config
  (winner-mode 1))

(which-function-mode 1)

(use-package beacon
  :diminish
  :config (beacon-mode 1))

(use-package discover-my-major
  :bind ("C-h C-m" . discover-my-major))

(use-package popup-kill-ring
  :bind ("M-y" . popup-kill-ring))

(use-package which-key
  :diminish
  :custom
  (which-key-separator " ")
  (which-key-prefix-prefix "+")
  :config
  (which-key-mode))

(use-package outline-magic
  :ensure t
  :bind (:map outline-minor-mode-map ([C-tab] . outline-cycle))
  :config
  (set-display-table-slot standard-display-table 'selective-display (string-to-vector "⤵")))

(use-package esup)
;; (use-package benchmark-init)
;; (use-package multiple-cursors)

(provide 'init-conf)
;;; init-conf ends here
