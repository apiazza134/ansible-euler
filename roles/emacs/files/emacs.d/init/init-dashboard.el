;;; init-dashboard.el -- dashboard configuration
;;; Commentary:

;;; Code:
(use-package recentf
  :ensure nil
  :hook (after-init . recentf-mode)
  :custom
  (recentf-auto-cleanup 'never)
  (recentf-exclude `(,(expand-file-name package-user-dir)
                     "recentf"
                     "COMMIT_EDITMSG\\'"
                     ,(expand-file-name "~/org/agenda/*")
                     ;; "^/\\(?:ssh\\|su\\|sudo\\)?:"
                     )))

(use-package dashboard
  :diminish (dashboard-mode page-breake-lines-mode)
  :init
  (add-hook 'after-init-hook 'dashboard-refresh-buffer)
  :commands (open-dashboard)
  :bind
  (:map dashboard-mode-map
   (("n" . dashboard-next-line)
    ("p" . dashboard-previous-line)
    ("N" . dashboard-next-section)
    ("F" . dashboard-previous-section)))
  :custom
  (dashboard-set-heading-icons t)
  (dashboard-set-navigator t)
  (dashboard-set-init-info t)
  (dashboard-center-content t)
  (dashboard-footer-messages nil)
  (dashboard-agenda-week t)
  (initial-buffer-choice (lambda () (get-buffer dashboard-buffer-name)))
  (dashboard-items '((recents  . 10) (projects . 5) (agenda . 3)))
  :config
  (defun open-dashboard ()
    "Open the *dashboard* buffer and jump to the first widget."
    (interactive)
    (if (get-buffer dashboard-buffer-name)
        (kill-buffer dashboard-buffer-name))
    (dashboard-insert-startupify-lists)
    (switch-to-buffer dashboard-buffer-name)
    (goto-char (point-min))
    (delete-other-windows))
  )

(dashboard-setup-startup-hook)

(provide 'init-dashboard)
;;; init-dashboard ends here
