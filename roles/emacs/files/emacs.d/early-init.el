;;; early-init.el

(setq gc-cons-threshold 100000000)
(setq package-enable-at-startup nil)

(defvar file-name-handler-alist-original file-name-handler-alist)
(setq file-name-handler-alist nil)

(setq site-run-file nil)

(push '(tool-bar-lines . 0) default-frame-alist)
(scroll-bar-mode 0)

(setq ring-bell-function 'ignore)

(setq frame-resize-pixelwise t)
(setq coding-system-for-read 'utf-8)

(provide 'early-init)
