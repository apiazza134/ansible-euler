;;; init.el -- Init file
;;; Commentary:

;;; Code:
;; Early init compatibility
(cond ((version< emacs-version "26.1")
       (warn "M-EMACS requires Emacs 26.1 and above!"))
      ((let* ((early-init-f (expand-file-name "early-init.el" user-emacs-directory))
              (early-init-do-not-edit-d (expand-file-name "early-init-do-not-edit/" user-emacs-directory))
              (early-init-do-not-edit-f (expand-file-name "early-init.el" early-init-do-not-edit-d)))
         (and (version< emacs-version "27")
              (or (not (file-exists-p early-init-do-not-edit-f))
                  (file-newer-than-file-p early-init-f early-init-do-not-edit-f)))
         (make-directory early-init-do-not-edit-d t)
         (copy-file early-init-f early-init-do-not-edit-f t t t t)
         (add-to-list 'load-path early-init-do-not-edit-d)
         (require 'early-init))))


;; Garbage collection
(defvar better-gc-cons-threshold 134217728) ; 128mb
(add-hook 'emacs-startup-hook
          (lambda ()
            (setq gc-cons-threshold better-gc-cons-threshold)
            (setq file-name-handler-alist file-name-handler-alist-original)
            (makunbound 'file-name-handler-alist-original)))
(add-hook 'emacs-startup-hook
          (lambda ()
            (if (boundp 'after-focus-change-function)
                (add-function :after after-focus-change-function
                              (lambda ()
                                (unless (frame-focus-state)
                                  (garbage-collect))))
              (add-hook 'after-focus-change-function 'garbage-collect))
            (defun gc-minibuffer-setup-hook ()
              (setq gc-cons-threshold (* better-gc-cons-threshold 2)))

            (defun gc-minibuffer-exit-hook ()
              (garbage-collect)
              (setq gc-cons-threshold better-gc-cons-threshold))

            (add-hook 'minibuffer-setup-hook #'gc-minibuffer-setup-hook)
            (add-hook 'minibuffer-exit-hook #'gc-minibuffer-exit-hook)))

(add-to-list 'load-path (expand-file-name "init" user-emacs-directory))

(require 'init-package)
(require 'init-const)
;; (require 'init-search)
(require 'init-keybind)
(require 'init-dired)
(require 'init-undotree)
(require 'init-buffer)
(require 'init-conf)
(require 'init-theme)
(require 'init-dashboard)
(require 'init-org)
(require 'init-markdown)
(require 'init-kube)
(require 'init-spell)

;; Programming
(add-to-list 'load-path (expand-file-name "init/prog" user-emacs-directory))
(require 'init-repos)
(require 'init-yasnippet)
(require 'init-syntax)
(require 'init-parens)
(require 'init-indent)
(require 'init-quickrun)
(require 'init-company)
(require 'init-cc)
(require 'init-python)
(require 'init-haskell)
(require 'init-latex)
(require 'init-web)
(require 'init-go)
(require 'init-fortran)
(require 'init-misc)

(provide 'init)
;;; init ends here
