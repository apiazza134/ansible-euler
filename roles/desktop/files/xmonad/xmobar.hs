Config
  {
    -- Appearece
    font = "xft:DejaVuSansMono-20"  -- get list `$ fc-list`
  , bgColor = "black"
  , fgColor = "white"
  , position = TopSize L 100 30
  -- , border = BottomB
  -- , borderColor = "red"

  -- Layout
  , sepChar = "%"
  , alignSep = "}{"
  , template = "%UnsafeStdinReader% }{ %dynnetwork% | %memory% | %multicpu% * %coretemp% | %battery% | %date%"

  -- General behavior
  , lowerOnStart = True  -- send to bottom of window stack on start
  , hideOnStart = False  -- start with window unmapped (hidden)
  , allDesktops = True  -- show on all desktops
  , overrideRedirect = False  -- set the Override Redirect flag (Xlib)
  , persistent = True  -- enable/disable hiding (True = disabled)

  -- Plugins
  , commands = [
      Run UnsafeStdinReader

      -- network activity monitor (dynamic interface resolution)
      , Run DynNetwork [ "--template", "<dev> UP:<tx> DOWN:<rx>", "--suffix", "True", "--ppad", "2"
                       , "--Low", "1000", "--High", "100000" -- units: B/s
                       , "--low", "darkgreen", "--normal", "darkorange", "--high", "darkred"
                       ] 10

        -- memory usage monitor
      , Run Memory [ "--template", "Mem: <usedratio>"
                   , "--suffix", "True"
                   , "--ppad", "2"
                   , "--Low", "20", "--High", "90"  -- units: %
                   , "--low", "darkgreen", "--normal", "darkorange", "--high", "darkred"
                   ] 10

        -- cpu activity monitor
      , Run MultiCpu [ "--template", "Cpu: <total>"
                     , "--suffix", "True"
                     , "--ppad", "2"
                     , "--Low", "30", "--High", "85" -- units: %
                     , "--low", "darkgreen", "--normal", "darkorange", "--high", "darkred"
                     ] 10

        -- cpu core temperature monitor
      , Run CoreTemp [ "--template", "Temp: <core0> <core1> <core2>"
                     , "--suffix", "True"
                     , "--Low", "40", "--High", "80" -- units: °C
                     , "--low", "darkgreen", "--normal", "darkorange", "--high", "darkred"
                     ] 50

      -- , Run MultiCoreTemp [] 50


        -- battery monitor
      , Run Battery [ "--template", "Batt: <acstatus>"
                    , "--suffix", "True"
                    , "--ppad", "2"
                    , "--Low", "10"
                    , "--low", "darkgreen", "--high", "darkorange"

                    , "--" -- battery specific options
                    , "-o", "<left> (<timeleft>)" -- discharging status
                    , "-O", "<fc=#dAA520>Charging</fc> <left>" -- AC "on" status
                    , "-i", "<fc=#006000>Charged</fc>" -- charged status
                    ] 3000

        -- time and date indicator
        --   (%F = y-m-d date, %a = day of week, %T = h:m:s time)
      , Run Date "<fc=#ee9a00>%F (%a) %T</fc>" "date" 10
      ]
  }
