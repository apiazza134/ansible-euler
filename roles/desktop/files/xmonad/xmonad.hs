{-# OPTIONS_GHC -Wno-deprecations #-}
import XMonad
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.SetWMName
import XMonad.Hooks.ManageHelpers
import XMonad.Layout.NoBorders
import XMonad.Util.Run(spawnPipe)
import XMonad.Util.EZConfig(additionalKeysP,removeKeysP)
import qualified XMonad.StackSet as W

import System.IO
import System.Exit

import CustomGnome
-- import XMonad.Config.Gnome

main :: IO ()
main = do
  -- spawn "feh --bg-fill ~/Pictures/wallpapers/3840x2160.png" -- Setting wallpaper
  xmproc <- spawnPipe "/usr/bin/xmobar ~/.xmonad/xmobar.hs"
  xmonad $ ewmh gnomeConfig
    {
      manageHook = composeAll [ manageDocks, isFullscreen --> doFullFloat, manageHook gnomeConfig ]
    , layoutHook = smartBorders $ avoidStruts $ layoutHook gnomeConfig
    , handleEventHook = composeAll [ handleEventHook gnomeConfig, docksEventHook, fullscreenEventHook ]
    , workspaces = myWorkspaces
    , logHook = dynamicLogWithPP xmobarPP
                {
                  ppOutput = hPutStrLn xmproc
                  -- clickable Tall/Mirror/Full
                , ppLayout = (\x -> "<action=`xdotool key Super_L+space`>" ++ x ++ "</action>")
                , ppTitle = xmobarColor "green" "" . shorten 30
                }
                >> ewmhDesktopsLogHook
    , modMask = mod4Mask  -- Rebind Mod to Windows key
    , terminal = "x-terminal-emulator"
    } `removeKeysP` myRemoveKeys `additionalKeysP` myKeys

-- clickable workspaces
myWorkspaces :: [String]
myWorkspaces = ["<action=`xdotool key Super_L+" ++ show n ++ "`>" ++ show n ++ "</action>" | n <- [1..9]]


myKeys :: [(String, X ())]
myKeys =
  [
    -- Lock/Logout
    ("M-l", spawn "gnome-screensaver-command -l") -- lock screen
  , ("M-S-e", spawn "gnome-session-quit") -- quit
  -- , ("M-S-e", io (exitWith ExitSuccess)) -- quit

    -- Window operations
  , ("M-S-q", kill) -- close focused window
  , ("M-<Up>", windows W.focusUp)
  , ("M-<Down>", windows W.focusDown)
  , ("M-S-<Return>", windows W.swapMaster)
  , ("M-S-<Up>", windows W.swapUp)
  , ("M-S-<Down>", windows W.swapDown)
  , ("M--", sendMessage Shrink) -- shriks the master area
  , ("M-+", sendMessage Expand) -- expand the master area

    -- Recompile and restart xmonad
  , ("M-S-r", spawn "if type xmonad; then xmonad --recompile && xmonad --restart; else xmessage xmonad not in \\$PATH: \"$PATH\"; fi")

    -- Run programs
  , ("M-<Return>", spawn $ XMonad.terminal gnomeConfig) -- lauch terminal
  , ("M-d", spawn "rofi -combi-modi drun,run -show combi -modi combi")
  , ("M-e", spawn "emacsclient --alternate-editor='' --create-frame")
  , ("M-g", spawn "x-www-browser")
  , ("M-f", spawn "nemo")
  , ("M-p", spawn "evince")
  , ("M-m", spawn "telegram-desktop")
  , ("M-s", spawn "scrot --select --file '/home/alepiazza/Pictures/Screenshots/%Y-%m-%d_%H%M%S_$wx$h_scrot.png' -e 'xclip -selection clipboard -target image/png -i $f'")
  ]


myRemoveKeys :: [String]
myRemoveKeys = [ "M-q", "M-S-c", "<M-S-<Return>", "M-j", "M-S-j", "M-k", "M-S-k", "M-p" ]
