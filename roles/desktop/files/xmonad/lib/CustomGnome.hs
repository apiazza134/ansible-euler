{-# OPTIONS_GHC -fno-warn-missing-signatures #-}

-- | source: http://hackage.haskell.org/package/xmonad-contrib-0.15/docs/src/XMonad.Config.Gnome.html#local-6989586621679317861

module CustomGnome (
    gnomeConfig,
    gnomeRegister,
    desktopLayoutModifiers
    ) where

import XMonad
import XMonad.Config.Desktop
import XMonad.Util.Run (safeSpawn)

import System.Environment (getEnvironment)

gnomeConfig = desktopConfig
    { startupHook = gnomeRegister >> startupHook desktopConfig }

-- | Register xmonad with gnome. 'dbus-send' must be in the $PATH with which
-- xmonad is started.
--
-- This action reduces a delay on startup only only if you have configured
-- gnome-session>=2.26: to start xmonad with a command as such:
--
-- > gconftool-2 -s /desktop/gnome/session/required_components/windowmanager xmonad --type string
gnomeRegister :: MonadIO m => m ()
gnomeRegister = io $ do
    x <- lookup "DESKTOP_AUTOSTART_ID" `fmap` getEnvironment
    whenJust x $ \sessionId -> safeSpawn "dbus-send"
            ["--session"
            ,"--print-reply=literal"
            ,"--dest=org.gnome.SessionManager"
            ,"/org/gnome/SessionManager"
            ,"org.gnome.SessionManager.RegisterClient"
            ,"string:xmonad"
            ,"string:"++sessionId]
